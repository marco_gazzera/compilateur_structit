CC=gcc
YC=yacc
FL=flex

FILE=test

TARGET=structitfrontend
TARGET2=structitbackend
compile:
	@$(YC) -d ./src/yaccFrontEnd.y
	@$(FL) ./src/lexFrontEnd.l
	@$(CC) lex.yy.c y.tab.c ./src/structure.c -lfl -o $(TARGET)
	@make -s clean
	@$(YC) -d ./src/yaccBackEnd.y
	@$(FL) ./src/lexBackEnd.l
	@$(CC) lex.yy.c y.tab.c -lfl -o $(TARGET2)
	@make -s clean
compile-fedora :
	@$(YC) -d ./src/yaccFrontEnd.y
	@$(FL) ./src/lexFrontEnd.l
	@$(CC) lex.yy.c y.tab.c ./src/structure.c -o $(TARGET) -fcommon
	@make -s clean
	@$(YC) -d ./src/yaccBackEnd.y
	@$(FL) ./src/lexBackEnd.l
	@$(CC) lex.yy.c y.tab.c -o $(TARGET2) -fcommon
	@make -s clean

clean:
	@rm y.tab.c y.tab.h lex.yy.c
	
clean_all_backend:
	@rm ./resultat_backend/*.c

run_test:
	@./testYacc < ./test/test.c

run_all:	
	@./src/bash/run_all.sh

run :
	@echo "---------------"
	@echo "Est entrain de compiler $(FILE) ..."
	@./$(TARGET) < ./test/$(FILE)
	@mkdir -p resultat_backend/
	@mv ./_tmp_file.c ./resultat_backend/backend_$(FILE)
	@echo "Fichier en backend généré dans ./resultat_backend/backend_$(FILE)"
	@echo
	@echo "Fichier backend en cours de vérification ... "
	@./$(TARGET2) < ./resultat_backend/backend_$(FILE)
	@echo
	@echo "Fichier backend ont été vérifié et valider par le parseur ! "
	@echo "Fin"
	@echo "---------------"

run_backend_only :
	@echo
	@echo "Fichier backend en cours de vérification ... "
	@./$(TARGET2) < ./resultat_backend/$(FILE)
	@echo "Fichier backend ont été vérifié et valider par le parseur ! "
	@echo "Fin"