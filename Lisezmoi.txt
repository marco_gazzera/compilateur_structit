# Compilateur : structitfrontend  structitbackend

##Comment compiler
* Avec Fedora :
	* make compile-fedora *
	*  ** Compile en utilisant les options nécessaires pour Fedora, le résultat est deux fichier executable structitfrontend et structitbackend **
* Avec Windows :
	* make compile *
	*  ** Compile en utilisant les options nécessaires pour Windows, le résultat est deux fichier executable structitfrontend et structitbackend ** 
		
## Comment lancer ce Compilateur
Pour lancer le compilateur : 
* Sur un fichier : *make run FILE=fichier.c*  
  *  **Compile fichier.c avec structitfrontend et ajoute backend_fichier.c dans resultat_backend, puis vérifie le lexique et la syntaxe de backend_fichier.c avec structitbackend **
* Sur tout les fichiers de test : *make run_all*  
  *  **Lance make run FILE= avec tout les fichier en .c se trouvant dans le dossier test**



